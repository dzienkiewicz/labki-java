public class Miasto {
    public Miasto(){
        kraj="pl";
        region="pl";
        kodPocztowy="pl";
        liczbaMieszkancow=0;
    }
    public Miasto(String kraj,String region, String kodPocztowy, int liczbaMieszkancow){
        this.liczbaMieszkancow=liczbaMieszkancow;
        this.kodPocztowy=kodPocztowy;
        this.region=region;
        this.kraj=kraj;
    }

    public String getKraj() {
        return kraj;
    }

    public String getRegion() {
        return region;
    }

    public String getKodPocztowy() {
        return kodPocztowy;
    }

    public int getLiczbaMieszkancow() {
        return liczbaMieszkancow;
    }


    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setKodPocztowy(String kodPocztowy) {
        this.kodPocztowy = kodPocztowy;
    }

    public void setLiczbaMieszkancow(int liczbaMieszkancow) {
        this.liczbaMieszkancow = liczbaMieszkancow;
    }
    public void printMiasto(){
        System.out.println(kraj+" "+region+" "+kodPocztowy+" "+liczbaMieszkancow);
    }
    private String kraj;
    private String region;
    private String kodPocztowy;
    private int liczbaMieszkancow;
}
